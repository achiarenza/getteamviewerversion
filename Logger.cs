﻿using System;
using System.IO;

namespace GetTVVersion
{
    public class Logger : IDisposable
    {
        public StreamWriter Wr { get; set; }

        public Logger()
        {
            Wr = File.CreateText(GetLogFileName());
        }

        public void Dispose()
        {
            Wr.Close();
            Wr.Dispose();
        }

        public void LogToLocalFile(string text)
        {
            Wr.WriteLine(TextLogFormat(text));
        }

        public string TextLogFormat(string text)
        {
            switch (text)
            {
                case "-":
                    return "-----------------------------------------------------------";
                case "":
                    return "\n";
                default:
                    return $"[{ DateTime.Now }] - { text }";
            }
        }

        public string GetLogFileName()
        {
            return $"{Directory.GetCurrentDirectory()}\\{AppDomain.CurrentDomain.FriendlyName.Replace(".exe", "")}_log.txt";
        }
    }
}
