﻿using System;
using System.Net;

namespace GetTVVersion
{
    class Program
    {
        private static readonly WebClient client = new WebClient();
        private static string url = "http://testscala.mcube.it/tv-migration/registry/save.do";

        static void Main()
        {
            try
            {
                using (Logger log = new Logger())
                {
                    try
                    {
                        PlayerData playerData = new PlayerData();

                        client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        string paramString = playerData.ToParamString();
                        log.LogToLocalFile(paramString);
                        log.LogToLocalFile(client.UploadString(url, paramString));
                        //log.LogToLocalFile(playerData.ToParamString());
                    }
                    catch (Exception ex)
                    {
                        log.LogToLocalFile(ex.Message);
                        log.LogToLocalFile(ex.StackTrace);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                //Console.ReadLine();
            }
        }
    }
}
