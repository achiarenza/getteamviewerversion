﻿using System.Xml.Linq;
using System.Xml.XPath;

namespace GetTVVersion
{
    class ScalaPlayerInfo
    {
        public string name { get; set; }
        public string cmHostname { get; set; }
        public string uuid { get; set; }
        XDocument doc;

        public ScalaPlayerInfo()
        {
            doc = LoadPlan();
            if (doc != null)
            {
                name = GetPlayerName();
                cmHostname = GetCmHostname();
                uuid = GetUuid();
            }            
        }

        private string GetUuid()
        {
            return doc.Root.XPathSelectElement("player/identifier").Value;
        }

        private string GetCmHostname()
        {
            return ParseCM(doc.Root.XPathSelectElement("content-manager-root/url").Value);
        }

        private string GetPlayerName()
        {
            return doc.Root.Element("player").Attribute("name").Value;
        }

        private XDocument LoadPlan()
        {
            if (System.IO.File.Exists("C:\\ProgramData\\Scala\\InfoChannel Player 5\\Network\\plan.xml"))
            {
                return XDocument.Load("C:\\ProgramData\\Scala\\InfoChannel Player 5\\Network\\plan.xml");
            }
            if (System.IO.File.Exists("D:\\ProgramData\\Scala\\InfoChannel Player 5\\Network\\plan.xml"))
            {
                return XDocument.Load("D:\\ProgramData\\Scala\\InfoChannel Player 5\\Network\\plan.xml");
            }

            return null;
        }

        private string ParseCM(string cm)
        {
            var temp = cm.Split('@');
            return $"{ temp[0].Split('/')[0] }//{ temp[1] }";
        }
    }
}
