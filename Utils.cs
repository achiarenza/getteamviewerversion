﻿using Microsoft.Win32;
using System;
using System.Net;
using System.Net.Sockets;

namespace GetTVVersion
{
    public static class Utils
    {      
        public static string GetTVVersion()
        {
            return HKLM_GetString(@"SOFTWARE\WOW6432Node\TeamViewer", "Version").Trim();
        }

        public static string GetOSVersion()
        {
            return FriendlyName();
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }

        public static string HKLM_GetString(string path, string key)
        {
            try
            {
                RegistryKey rk = Registry.LocalMachine.OpenSubKey(path);
                if (rk == null) return "";
                return (string)rk.GetValue(key);
            }
            catch { return ""; }
        }

        public static string FriendlyName()
        {
            string ProductName = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName");
            string CurrentBuild = HKLM_GetString(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion", "CurrentBuild");
            if (ProductName != "")
            {
                return $"{(ProductName.StartsWith("Microsoft") ? "" : "Microsoft")} {ProductName} {(CurrentBuild != "" ? $"Build {CurrentBuild}" : "")}";
            }
            return "";
        }

        public static string GetTVinstaller()
        {
            string[] dirs = { "C:\\", "C:\\teamviewer_unattended\\" };
            string res = "Not Found";

            foreach (string dir in dirs)
            {
                if (System.IO.Directory.Exists(dir))
                {
                    foreach (string file in System.IO.Directory.GetFiles(dir, "*.msi"))
                    {
                        if (file.Contains("TeamViewer"))
                        {
                            res = file;
                        }
                    }
                }                
            }

            return res;
        }
    }
}
