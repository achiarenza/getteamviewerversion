﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace GetTVVersion
{
    class PlayerData 
    {
        public string teamviewerVersion { get; set; }
        public string osVersion { get; set; }
        public string cmHostname { get; set; }
        public string playerUuid { get; set; }
        public string playerName { get; set; }
        public string playerIpAddress { get; set; }
        public string teamviewerInstaller { get; set; }

        public PlayerData()
        {
            teamviewerVersion = Utils.GetTVVersion();
            osVersion = Utils.GetOSVersion();
            playerIpAddress = Utils.GetLocalIPAddress();
            teamviewerInstaller = Utils.GetTVinstaller();
            ScalaPlayerInfo scalaPlayerInfo = new ScalaPlayerInfo();
            cmHostname = scalaPlayerInfo.cmHostname;
            playerName = scalaPlayerInfo.name;
            playerUuid = scalaPlayerInfo.uuid;            
        }

        public string ToParamString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (PropertyInfo p in this.GetType().GetProperties())
            {
                if (sb.Length != 0)
                {
                    sb.Append("&");
                }
                sb.Append($"{p.Name}={p.GetValue(this, null)}");
            }
            return sb.ToString();
        }
    }
}
